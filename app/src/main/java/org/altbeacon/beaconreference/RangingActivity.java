package org.altbeacon.beaconreference;

import java.util.Collection;
import java.util.HashMap;

import android.app.Activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.altbeacon.beacon.AltBeacon;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

public class RangingActivity extends Activity implements BeaconConsumer {
    protected static final String TAG = "RangingActivity";
    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
    private HashMap<String,String> beaconList = new HashMap<String,String>();
    private boolean buttonReleased = true;
    private int NotificationID = 0;
    private ToggleButton inRange;

    private SeekBar distanceBar;
    private TextView inRangeBelowBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranging);
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
                        beaconManager.bind(this);
        beaconManager.bind(this);
        Log.d("connected","startet");
        inRange =(ToggleButton)RangingActivity.this.findViewById(R.id.toggleButton2);
        inRange.setClickable(false);
        inRangeBelowBar =(TextView) RangingActivity.this.findViewById(R.id.textView6);
        inRangeBelowBar.setVisibility(View.VISIBLE);
        distanceBar =(SeekBar) RangingActivity.this.findViewById(R.id.seekBar);
        distanceBar.setVisibility(View.VISIBLE);

    }

    @Override 
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    @Override 
    protected void onPause() {
        super.onPause();
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(true);
    }

    @Override 
    protected void onResume() {
        super.onResume();
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(false);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                Log.d("Beacon Service", String.valueOf(beacons.size()));

                if (beacons.size() > 0) {
                    inRangeBelowBar.setVisibility(View.INVISIBLE);
                    distanceBar.setVisibility(View.VISIBLE);
                    Beacon firstBeacon = beacons.iterator().next();
                    Log.d("Beacon UUID", String.valueOf(firstBeacon.getId1()));
                    if(beaconList.containsKey(firstBeacon.getId1().toString())){
                        //logToDisplay("Beacon already there");
                        beaconInRange();
                        setDistance(firstBeacon.getDistance());
                        String storedMajor = beaconList.get(firstBeacon.getId1().toString());
                        if(storedMajor.equals("")){
                            beaconList.put(firstBeacon.getId1().toString(),storedMajor);
                        }
                        else if(!storedMajor.equals(firstBeacon.getId2().toString())){
                            if(buttonReleased){
                                buttonReleased = false;
                                startWebview();
                                sendNotification();
                                //logToDisplay("\nclicked");
                            }
                            else{
                                buttonReleased = true;
                                //logToDisplay("and released");
                            }
                            beaconList.put(firstBeacon.getId1().toString(),firstBeacon.getId2().toString());
                        }
                    }
                    else{
                        inRangeBelowBar.setVisibility(View.VISIBLE);
                        distanceBar.setVisibility(View.INVISIBLE);
                        //logToDisplay("added Beacon");
                        beaconList.put(firstBeacon.getId1().toString(),firstBeacon.getId2().toString());
                    };
                }
            }
        });
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {   }
    }
    public void startWebview() {
        Log.d("distance","setValue");
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("content", "abc");
        startActivity(intent);
    }
    private void setDistance(Double distance){
        if(distance >1.0) distance = 1.0;
        double distanceVal = distance *100;
        distanceBar.setProgress((int)distanceVal);
    }
    private void beaconInRange(){
        inRange.setChecked(true);
    }
   /* private void logToDisplay(final String line) {
        runOnUiThread(new Runnable() {
            public void run() {
                EditText editText = (EditText)RangingActivity.this.findViewById(R.id.rangingText);
                editText.append(line+"\n");
            }
        });
    }*/
    private void sendNotification(){
        createNotificationChannel();
        NotificationID = NotificationID++;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(new Intent(this, WebViewActivity.class));
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "pushChannel")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("Yoma App")
                .setContentText("Schlüsselfinder möchte Webseite öffnen")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(NotificationID, mBuilder.build());
    }
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "pushChannel";
            String description = "pushChannel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("pushChannel", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
